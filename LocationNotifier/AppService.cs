﻿using System;
using Android.App;
using Android.Media;
using Android.Content;
using Android.OS;
using Android.Net.Wifi;
using Android.Net;

namespace LocationNotifier
{
	public class AppService
	{
		public AppService ()
		{
		}
	}

	[Service]
	[IntentFilter(new[] { ActionPlay, ActionPause, ActionStop })]
	public class StreamingBackgroundService : Service, AudioManager.IOnAudioFocusChangeListener
	{
		//Actions
		public const string ActionPlay = "info.simsdev.outerpixel.PLAY";
		public const string ActionPause = "info.simsdev.outerpixel.PAUSE";
		public const string ActionStop = "info.simsdev.outerpixel.STOP";

		private MediaPlayer player;
		private AudioManager audioManager;
		private WifiManager wifiManager;
		private WifiManager.WifiLock wifiLock;
		private bool paused;
		private const int NotificationId = 76494;

		/// <summary>
		/// On create simply detect some of our managers
		/// </summary>
		public override void OnCreate()
		{
			base.OnCreate();
			//Find our audio and notificaton managers
			audioManager = (AudioManager)GetSystemService(AudioService);
			wifiManager = (WifiManager)GetSystemService(WifiService);
		}

		/// <summary>
		/// Don't do anything on bind
		/// </summary>
		/// <param name="intent"></param>
		/// <returns></returns>
		public override IBinder OnBind(Intent intent)
		{
			return null;
		}
			
		private async void Play(string link)
		{
			if (paused && player != null) {
				paused = false;
				//We are simply paused so just start again
				player.Start();
				StartForeground();
				return;
			}

			if (player == null) {
				IntializePlayer();
			}

			if (player.IsPlaying)
				return;

			try {
				await player.SetDataSourceAsync(ApplicationContext, Android.Net.Uri.Parse(link));

				var focusResult = audioManager.RequestAudioFocus(this, Stream.Music, AudioFocus.Gain);
				if (focusResult != AudioFocusRequest.Granted)
				{
					//could not get audio focus
					Console.WriteLine("Could not get audio focus");
				}

				player.PrepareAsync();
				AquireWifiLock();
				StartForeground();
			}
			catch (Exception ex) {
				//unable to start playback log error
				Console.WriteLine("Unable to start playback: " + ex);
			}
		}

		private void Pause()
		{
			if (player == null)
				return;

			if(player.IsPlaying)
				player.Pause();

			StopForeground(true);
			paused = true;
		}

		private void Stop()
		{
			if (player == null)
				return;

			if(player.IsPlaying)
				player.Stop();

			player.Reset();
			paused = false;
			StopForeground(true);
			ReleaseWifiLock();
		}

		public override StartCommandResult OnStartCommand(Intent intent, StartCommandFlags flags, int startId) 
		{
			switch (intent.Action) {
			case ActionPlay: Play(intent.GetStringExtra("urlLink")); break;
			case ActionStop: Stop(); break;
			case ActionPause: Pause(); break;
			}
			//Set sticky as we are a long running operation
			return StartCommandResult.Sticky;
		}

		private void IntializePlayer()
		{
			player = new MediaPlayer();

			//Tell our player to sream music
			player.SetAudioStreamType(Stream.Music);

			//Wake mode will be partial to keep the CPU still running under lock screen
			player.SetWakeMode(ApplicationContext, WakeLockFlags.Partial);

			//When we have prepared the song start playback
			player.Prepared += (sender, args) => player.Start();

			//When we have reached the end of the song stop ourselves, however you could signal next track here.
			player.Completion += (sender, args) => Stop();

			player.Error += (sender, args) =>
			{
				//playback error
				Console.WriteLine("Error in playback resetting: " + args.What);
				Stop();//this will clean up and reset properly.
			};
		}

		/// <summary>
		/// Lock the wifi so we can still stream under lock screen
		/// </summary>
		private void AquireWifiLock()
		{
			if (wifiLock == null){
				wifiLock = wifiManager.CreateWifiLock(WifiMode.Full, "xamarin_wifi_lock");
			} 
			wifiLock.Acquire();
		}

		/// <summary>
		/// This will release the wifi lock if it is no longer needed
		/// </summary>
		private void ReleaseWifiLock()
		{
			if (wifiLock == null)
				return;

			wifiLock.Release();
			wifiLock = null;
		}

		/// <summary>
		/// Properly cleanup of your player by releasing resources
		/// </summary>
		public override void OnDestroy()
		{
			base.OnDestroy();
			if (player != null)
			{
				player.Release();
				player = null;
			}
		}

		/// <summary>
		/// For a good user experience we should account for when audio focus has changed.
		/// There is only 1 audio output there may be several media services trying to use it so
		/// we should act correctly based on this.  "duck" to be quiet and when we gain go full.
		/// All applications are encouraged to follow this, but are not enforced.
		/// </summary>
		/// <param name="focusChange"></param>
		public void OnAudioFocusChange(AudioFocus focusChange)
		{
			switch (focusChange)
			{
			case AudioFocus.Gain:
				if (player == null)
					IntializePlayer();

				if (!player.IsPlaying)
				{
					player.Start();
					paused = false;
				}

				player.SetVolume(1.0f, 1.0f);//Turn it up!
				break;
			case AudioFocus.Loss:
				//We have lost focus stop!
				Stop();
				break;
			case AudioFocus.LossTransient:
				//We have lost focus for a short time, but likely to resume so pause
				Pause();
				break;
			case AudioFocus.LossTransientCanDuck:
				//We have lost focus but should till play at a muted 10% volume
				if(player.IsPlaying)
					player.SetVolume(.1f, .1f);//turn it down!
				break;

			}
		}

		/// <summary>
		/// When we start on the foreground we will present a notification to the user
		/// When they press the notification it will take them to the main page so they can control the music
		/// </summary>
		private void StartForeground()
		{

			var pendingIntent = PendingIntent.GetActivity(ApplicationContext, 0,
				new Intent(ApplicationContext, typeof(MainActivity)),
				PendingIntentFlags.UpdateCurrent);

			var notification = new Notification
			{
				TickerText = new Java.Lang.String("Song started!"),
				Icon = Resource.Drawable.play_circle
			};
			notification.Flags |= NotificationFlags.OngoingEvent;
			notification.SetLatestEventInfo(ApplicationContext, "Xamarin Streaming",
				"Playing music!", pendingIntent);
			StartForeground(NotificationId, notification);
		}
	}

	[BroadcastReceiver]
	[IntentFilter(new []{AudioManager.ActionAudioBecomingNoisy})]
	public class MusicBroadcastReceiver: BroadcastReceiver {
		public override void OnReceive(Context context, Intent intent){
			if (intent.Action != AudioManager.ActionAudioBecomingNoisy)
				return;
			//signal the service to stop!
			var stopIntent = new Intent(StreamingBackgroundService.ActionStop);
			context.StartService(stopIntent);
		}
	}
}

