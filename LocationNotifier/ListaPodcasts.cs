﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Support.V7.Widget;
using Android.Support.V7.App;
using Squareup.Picasso;
using Android.Support.Design.Widget;
using System.Threading.Tasks;
using System.Net.Http;

namespace LocationNotifier
{
	[Activity (Label = "Lista Podcasts", Theme = "@style/MyTheme")]
	public class ListaPodcasts : AppCompatActivity
	{
		CollapsingToolbarLayout collapsingToolbarLayout;
		RecyclerView mRecyclerView;
		RecyclerView.LayoutManager mLayoutManager;
		PodcastsAdapter mAdapter;
		List<Podcasts> mPodcasts;
		private ProgressDialog progressDialog = null;

		protected async override void OnCreate (Bundle savedInstanceState)
		{
			base.OnCreate (savedInstanceState);
			mPodcasts = new List<Podcasts>();
			SetContentView (Resource.Layout.ListaPodcasts);
			mRecyclerView = FindViewById<RecyclerView> (Resource.Id.rv);
			var musicBar = FindViewById<RelativeLayout> (Resource.Id.notificationMusicPlayer);

			if(musicBar != null)
				musicBar.Visibility = ViewStates.Invisible;

			progressDialog = ProgressDialog.Show(this, "Aguarde...", "Carregando Podcasts...", true);
			progressDialog.Show();

			Android.Support.V7.Widget.Toolbar toolbar = (Android.Support.V7.Widget.Toolbar) FindViewById(Resource.Id.toolbar);
			toolbar.SetNavigationIcon (Resource.Drawable.ic_arrow_back_white18);
			toolbar.Click += (sender, e) => {
				this.Finish();
			};
			toolbar.SetLogo (Resource.Drawable.OutercastLogoTitle);
				
			//this.FindViewById<ImageView> (Resource.Id.ivAvatar).SetImageResource (Resource.Drawable.OUTERCAST_COVER);
			Picasso.With(this).Load("http://outerpixel.com.br/wp-content/uploads/2016/02/OUTER-CAST-COVER.jpg").Into(this.FindViewById<ImageView>(Resource.Id.podcastImage));

			collapsingToolbarLayout =  FindViewById<CollapsingToolbarLayout>(Resource.Id.collapsingToolbarLayout);
			collapsingToolbarLayout.SetTitle("");

			// Plug in the linear layout manager:
			mLayoutManager = new LinearLayoutManager (this);
			mRecyclerView.SetLayoutManager (mLayoutManager);

			await PreencheItensLista ();
			for (int i = 0; i < 3; i++)
				CriaDadosFakePodcast (mPodcasts);

			// Plug in my adapter:
			mAdapter = new PodcastsAdapter (this, mPodcasts);
			mRecyclerView.SetAdapter (mAdapter);

			progressDialog.Dismiss();
		}
			
		protected async Task PreencheItensLista()
		{
			string FeedUrl = "http://outerpixel.com.br/category/podcasts/feed";
			try
			{
				using (var client = new HttpClient())
				{
					var xmlFeed = await client.GetStringAsync(FeedUrl);
					var doc = XDocument.Parse(xmlFeed);
					XNamespace dc = "http://purl.org/dc/elements/1.1/";
					XNamespace ns = "http://www.itunes.com/dtds/podcast-1.0.dtd";

					mPodcasts = (from item in doc.Descendants("item")
						select new Podcasts
						{
							Titulo = item.Element("title").Value,
							Data = Convert.ToDateTime(item.Element("pubDate").Value),
							PostedBy = item.Element(ns+"author").Value,
							Arquivo = item.Elements("enclosure").Attributes("url").SingleOrDefault().ToString().Replace("\"", "").Replace("url=", ""),
							Descricao = item.Element(ns+"subtitle").Value,
							Duracao = item.Element(ns+"duration").Value
						}).ToList();
				}
			}
			catch (Exception e)
			{
				//throw e;
				//this.Exception = e;
			}
		}

		public void CriaDadosFakePodcast(List<Podcasts> podcasts)
		{
			podcasts.Add (new Podcasts {
				Id = podcasts.Count.ToString(),
				Titulo = "Podcast #"+(podcasts.Count+1),
				Descricao = "Este é o Podcast de número "+(podcasts.Count+1),
				PostedBy = "Lucas Sims",
				Duracao = "60:20",
				Data = new DateTime(2016, 05, 21),
				Arquivo = "/sdcard0/App/teste.mp3"
			});
			podcasts.Add (new Podcasts {
				Id = podcasts.Count.ToString(),
				Titulo = "Podcast #"+(podcasts.Count+1),
				Descricao = "Este é o Podcast de número "+(podcasts.Count+1),
				PostedBy = "Lucas Sims",
				Duracao = "55:27",
				Data = new DateTime(2016, 04, 11),
				Arquivo = "/sdcard0/App/teste.mp3"
			});
			podcasts.Add (new Podcasts {
				Id = podcasts.Count.ToString(),
				Titulo = "Podcast #"+(podcasts.Count+1),
				Descricao = "Este é o Podcast de número "+(podcasts.Count+1),
				PostedBy = "Lucas Sims",
				Duracao = "33:12",
				Data = new DateTime(2016, 03, 05),
				Arquivo = "/sdcard0/App/teste.mp3"
			});
			podcasts.Add (new Podcasts {
				Id = podcasts.Count.ToString(),
				Titulo = "Podcast #"+(podcasts.Count+1),
				Descricao = "Este é o Podcast de número "+(podcasts.Count+1),
				PostedBy = "Lucas Sims",
				Duracao = "44:59",
				Data = new DateTime(2016, 02, 29),
				Arquivo = "/sdcard0/App/teste.mp3"
			});
			podcasts.Add (new Podcasts {
				Id = podcasts.Count.ToString(),
				Titulo = "Podcast #"+(podcasts.Count+1),
				Descricao = "Este é o Podcast de número "+(podcasts.Count+1),
				PostedBy = "Lucas Sims",
				Duracao = "01:15",
				Data = new DateTime(2016, 01, 17),
				Arquivo = "/sdcard0/App/teste.mp3"
			});
		}
	}
}

