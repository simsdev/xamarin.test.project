﻿using Android.App;
using Android.Widget;
using Android.OS;
using Android.Content;
using System;
using Android.Net;

namespace LocationNotifier
{
	[Activity (Label = "@string/app_name", MainLauncher = true, Icon = "@mipmap/icon")]
	public class MainActivity : Activity
	{
		int count = 1;

		protected override void OnCreate (Bundle savedInstanceState)
		{
			base.OnCreate (savedInstanceState);

			// Set our view from the "main" layout resource
			SetContentView (Resource.Layout.Main);

			// Get our button from the layout resource,
			// and attach an event to it
			Button button = FindViewById<Button> (Resource.Id.myButton);
			
			button.Click += delegate {
				button.Text = string.Format ("{0} clicks!", count++);
			};

			Button buttonWhatsapp = FindViewById<Button> (Resource.Id.buttonWhatsApp);

			/*buttonWhatsapp.Click += delegate {
				var uri = Android.Net.Uri.Parse("smsto:" + "11980566262");
				Intent sendIntent = new Intent(Intent.ActionSend, uri);
				//sendIntent.SetAction(Intent.ActionSend);
				sendIntent.PutExtra(Intent.ExtraText, "Teste "+DateTime.Now.ToString("dd/MM/yyyy HH:mm"));
				sendIntent.SetType("text/plain");
				sendIntent.SetPackage("com.whatsapp");
				StartActivity(sendIntent);
			};*/

			Button buttonPC = FindViewById<Button> (Resource.Id.buttonListaPodcast);

			buttonPC.Click += delegate {
				var intent = new Intent(this, typeof(ListaPodcasts));
				StartActivity(intent);
			};

			Button buttonLB = FindViewById<Button> (Resource.Id.buttonListaBluetooth);

			buttonLB.Click += delegate {
				var intent = new Intent(this, typeof(ListaBluetooth));
				StartActivity(intent);
			};

		}
	}
}


