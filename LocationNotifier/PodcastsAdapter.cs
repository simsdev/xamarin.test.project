﻿using System;
using System.Collections.Generic;
using System.Linq;
using Android.Support.V7.Widget;
using Android.Widget;
using Android.Views;
using System.Globalization;
using Android.Content;

namespace LocationNotifier
{
	public class PodcastsAdapter : RecyclerView.Adapter
	{
		public Context context;
		public List<Podcasts> mPodcasts;
		public PodcastsAdapter (Context con, List<Podcasts> podcasts)
		{
			context = con;
			mPodcasts = podcasts;
		}

		public override long GetItemId(int position)
		{
			return position;
		}

		public override RecyclerView.ViewHolder OnCreateViewHolder (ViewGroup parent, int viewType)
		{
			View itemView = LayoutInflater.From (parent.Context).
				Inflate (Resource.Layout.list_rowPodcast, parent, false);

			PodcastsViewHolder vh = new PodcastsViewHolder (itemView, mPodcasts, context);
			return vh;
		}

		public override void OnBindViewHolder (RecyclerView.ViewHolder holder, int position)
		{
			PodcastsViewHolder vh = holder as PodcastsViewHolder;
			//vh.BotaoPlay.SetImageResource (mPodcasts[position].PhotoID);
			vh.Dia.Text = mPodcasts[position].Data.Day.ToString();
			vh.Mes.Text = CultureInfo.CurrentCulture.DateTimeFormat.GetAbbreviatedMonthName(mPodcasts[position].Data.Month);
			vh.Titulo.Text = mPodcasts[position].Titulo;
			vh.Descricao.Text = "";//mPodcasts[position].Descricao;
			vh.PostedBy.Text = mPodcasts[position].PostedBy;
			vh.Duracao.Text = mPodcasts[position].Duracao;
			vh.Id.Text = mPodcasts [position].Id;

			if (!mPodcasts[position].IsDownloaded)
				vh.BotaoPlay.SetImageResource (Resource.Drawable.download_circle);
			else {
				if(mPodcasts[position].IsClicked) 
					vh.BotaoPlay.SetImageResource(Resource.Drawable.pause_circle);
				else
					vh.BotaoPlay.SetImageResource(Resource.Drawable.play_circle);
			}
		}



		public override int ItemCount
		{
			get { return mPodcasts.Count; }
		}
	}

	public class PodcastsViewHolder : RecyclerView.ViewHolder
	{
		public ImageButton BotaoPlay { get; private set; }
		public TextView Mes { get; private set; }
		public TextView Dia { get; private set; }
		public TextView Titulo { get; private set; }
		public TextView Descricao { get; private set; }
		public TextView PostedBy { get; private set; }
		public TextView Duracao { get; private set; }
		public TextView Id { get; private set; }
		public int LocalPosition { get; private set; }

		public PodcastsViewHolder (View itemView, List<Podcasts> mPodcasts, Context context) : base (itemView)
		{
			// Locate and cache view references:
			BotaoPlay = itemView.FindViewById<ImageButton> (Resource.Id.playButton);
			Mes = itemView.FindViewById<TextView> (Resource.Id.monthMsg);
			Dia = itemView.FindViewById<TextView> (Resource.Id.dayMsg);
			Titulo = itemView.FindViewById<TextView> (Resource.Id.title);
			Descricao = itemView.FindViewById<TextView> (Resource.Id.subtitle);
			PostedBy = itemView.FindViewById<TextView> (Resource.Id.postedby);
			Duracao = itemView.FindViewById<TextView> (Resource.Id.duration);
			Id = itemView.FindViewById<TextView> (Resource.Id.idItem);

			BotaoPlay.Click += (sender, e) => {
				if(!mPodcasts[base.AdapterPosition].IsDownloaded)
				{
					mPodcasts[base.AdapterPosition].IsDownloaded = true;
					(sender as ImageButton).SetImageResource(Resource.Drawable.play_circle);
				}
				else
				{
					if(!mPodcasts[base.AdapterPosition].IsClicked) 
					{
						mPodcasts[base.AdapterPosition].IsClicked = true;
						(sender as ImageButton).SetImageResource(Resource.Drawable.pause_circle);

						var intent = new Intent(StreamingBackgroundService.ActionPlay);
						intent.PutExtra("urlLink", mPodcasts[base.AdapterPosition].Arquivo);
						context.StartService(intent);
					}
					else
					{
						mPodcasts[base.AdapterPosition].IsClicked = false;
						(sender as ImageButton).SetImageResource(Resource.Drawable.play_circle);

						var intent = new Intent(StreamingBackgroundService.ActionPause);
						context.StartService(intent);
					}
				}
			};
		}
	}

	public class Podcasts
	{
		public string Id {get; set;}
		public string Titulo {get;set;}
		public string Descricao {get;set;}
		public string PostedBy {get;set;}
		public string Duracao {get;set;}
		public DateTime Data {get;set;}
		public string Arquivo {get;set;}
		public bool IsClicked {get;set;}
		public bool IsDownloaded { get; set; }
	}
}

