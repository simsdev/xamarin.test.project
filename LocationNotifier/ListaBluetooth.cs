﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Support.V7.Widget;
using Android.Support.V7.App;
using Squareup.Picasso;
using Android.Support.Design.Widget;
using System.Threading.Tasks;
using System.Net.Http;
using Android.Bluetooth;
using Java.Lang.Reflect;
using Java.Util;

namespace LocationNotifier
{
    [Activity(Label = "Lista Bluetooth")]
    public class ListaBluetooth : Activity
    {
        private ListView listView;
        private List<String> mDeviceList = new List<String>();
        private BluetoothAdapter mBluetoothAdapter;
        private BTReceiver mReceiver;
        private ProgressDialog progressDialog = null;
        private Android.App.AlertDialog dialog;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.ListaGenerica);
            listView = FindViewById<ListView>(Resource.Id.listViewGenerica);

            progressDialog = ProgressDialog.Show(this, "Aguarde...", "Carregando Bluetooth...", true);
            progressDialog.Show();

            mBluetoothAdapter = BluetoothAdapter.DefaultAdapter;
            mBluetoothAdapter.StartDiscovery();

            // Get a set of currently paired devices
            var pairedDevices = mBluetoothAdapter.BondedDevices;

            // If there are paired devices, add each one to the ArrayAdapter
            if (pairedDevices.Count > 0)
            {
                foreach (var device in pairedDevices)
                {
                    mDeviceList.Add("[PAIRED] " + device.Name + "\n" + device.Address);
                }
            }
            else
            {

            }

            mReceiver = new BTReceiver(mDeviceList, listView);
            IntentFilter filter = new IntentFilter(BluetoothDevice.ActionFound);
            RegisterReceiver(mReceiver, filter);

            filter = new IntentFilter(BluetoothAdapter.ActionDiscoveryFinished);
            RegisterReceiver(mReceiver, filter);

            filter = new IntentFilter(BluetoothDevice.ActionPairingRequest);
            RegisterReceiver(mReceiver, filter);

            filter = new IntentFilter(BluetoothDevice.ActionBondStateChanged);
            RegisterReceiver(mReceiver, filter);

            var inputView = LayoutInflater.Inflate(Resource.Layout.connectBluetoothManual, null);
            var builder = new Android.App.AlertDialog.Builder(this);
            builder.SetTitle("Connect Manually");
            builder.SetView(inputView);
            builder.SetPositiveButton("Connect", connectClicked);
            builder.SetNegativeButton("Cancel", delegate { dialog.Dismiss(); });
            dialog = builder.Create();

            //EditText macAddress = dialog.FindViewById<EditText>(Resource.Id.inputMac);
            //macAddress.TextChanged += (sender, ev) => {
            //	macAddress.Text = formatMacAddress(macAddress.Text);
            //};

            var buttonAdd = FindViewById<Button>(Resource.Id.buttonPairBluetooth);
            buttonAdd.Click += (s, e) =>
            {
                dialog.Show();
            };

            listView.ItemClick += (object sender, AdapterView.ItemClickEventArgs e) =>
            {
                var selectedFromList = listView.GetItemAtPosition(e.Position).ToString();
                var splitted = selectedFromList.Replace("\n", "|").Split('|');
                ConnectToBluetooth(splitted[1], "0000");
            };

            progressDialog.Dismiss();
        }

        protected override void OnDestroy()
        {
            // Make sure we're not doing discovery anymore
            if (mBluetoothAdapter != null)
            {
                mBluetoothAdapter.CancelDiscovery();
            }

            UnregisterReceiver(mReceiver);
            base.OnDestroy();
        }

        private void connectClicked(object sender, DialogClickEventArgs e)
        {
            //SetContentView(Resource.Layout.connectBluetoothManual);

            EditText macAddress = (sender as Android.App.AlertDialog).FindViewById<EditText>(Resource.Id.inputMac);
            EditText pin = (sender as Android.App.AlertDialog).FindViewById<EditText>(Resource.Id.inputPin);

            if (formatMacAddress(macAddress.Text).Length < 17)
            {
                dialog.Dismiss();
                ShowDialogMsg("Erro!", "MAC Address tem menos de 17 caracteres");
            }
            else
            {
                if (macAddress.Text.Length < 17)
                    macAddress.Text = formatMacAddress(macAddress.Text);

                ConnectToBluetooth(macAddress.Text, pin.Text);
                dialog.Dismiss();
            }
        }

        private void ConnectToBluetooth(string macAddress, string pin = "")
        {
            try
            {
                BluetoothDevice remoteDevice = mBluetoothAdapter.GetRemoteDevice(macAddress);
                byte[] bytePin = (byte[])System.Text.Encoding.UTF8.GetBytes(pin);
                remoteDevice.SetPin(bytePin);
                var bond = remoteDevice.CreateBond();
            }
            catch (Exception ex)
            {
                ShowDialogMsg("Erro!", (ex.InnerException != null ? ex.InnerException.Message : ex.Message));
            }
        }

        private String formatMacAddress(String cleanMac)
        {
            int grouppedCharacters = 0;
            String formattedMac = "";

            for (int i = 0; i < cleanMac.Length; ++i)
            {
                formattedMac += cleanMac[i];
                ++grouppedCharacters;

                if (grouppedCharacters == 2)
                {
                    formattedMac += ":";
                    grouppedCharacters = 0;
                }
            }

            // Removes trailing colon for complete MAC address
            if (cleanMac.Length == 12)
                formattedMac = formattedMac.Substring(0, formattedMac.Length - 1);

            return formattedMac;
        }

        private String clearNonMacCharacters(String mac)
        {
            return mac.ToString().Replace("[^A-Fa-f0-9]", "");
        }

        /// <summary>
        /// Start device discover with the BluetoothAdapter
        /// </summary>
        private void DoDiscovery()
        {
            // Indicate scanning in the title
            SetProgressBarIndeterminateVisibility(true);
            this.Title = "Buscando por dispositivos";

            // If we're already discovering, stop it
            if (mBluetoothAdapter.IsDiscovering)
            {
                mBluetoothAdapter.CancelDiscovery();
            }

            // Request discover from BluetoothAdapter
            mBluetoothAdapter.StartDiscovery();
        }

        private void ShowDialogMsg(string titulo, string message, bool showBTDialog = false)
        {
            Android.App.AlertDialog.Builder builderMsg = new Android.App.AlertDialog.Builder(this);
            builderMsg.SetTitle(titulo);
            builderMsg.SetMessage(message);
            builderMsg.SetCancelable(false);
            builderMsg.SetPositiveButton("OK", delegate
            {
                builderMsg.Dispose();
                if (showBTDialog)
                    dialog.Show();
            });
            builderMsg.Show();
        }
    }

    public class BTReceiver : BroadcastReceiver
    {
        List<string> mDeviceList;
        ListView mListView;

        public BTReceiver(List<string> deviceList, ListView listView)
        {
            this.mDeviceList = deviceList;
            this.mListView = listView;
        }

        public override void OnReceive(Context context, Intent intent)
        {
            string action = intent.Action;
            if (action == BluetoothDevice.ActionFound)
            {
                BluetoothDevice device = (BluetoothDevice)intent.GetParcelableExtra(BluetoothDevice.ExtraDevice);
                mDeviceList.Add("[FOUND] " + device.Name + "\n" + device.Address);
                // "BT", device.Name + "\n" + device.Address
                mListView.Adapter = new ArrayAdapter<String>(context, Android.Resource.Layout.SimpleListItem1, mDeviceList);
            }
            else if (action == BluetoothAdapter.ActionDiscoveryFinished)
            {

            }
            else if (action == BluetoothDevice.ActionPairingRequest)
            {
                BluetoothDevice device = (BluetoothDevice)intent.GetParcelableExtra(BluetoothDevice.ExtraDevice);
                try
                {
                    //Method m = device.Class.GetMethod("setPin");
                    //m.Invoke(device, pin);
                    //device.Class.GetMethod("setPairingConfirmation").Invoke(device, true);
                    device.SetPairingConfirmation(true);
                }
                catch (Exception e)
                {
                }
            }
            else if (action == BluetoothDevice.ActionBondStateChanged)
            {
                BluetoothDevice device = (BluetoothDevice)intent.GetParcelableExtra(BluetoothDevice.ExtraDevice);
                if (device.BondState == Bond.Bonded)
                {
                    try
                    {
                        UUID SERIAL_UUID = UUID.FromString("00001101-0000-1000-8000-00805F9B34FB");
                        var socket = device.CreateRfcommSocketToServiceRecord(SERIAL_UUID);
                        socket.Connect();
                    }
                    catch (Exception ex)
                    {
                    }
                }
            }
        }
    }
}

